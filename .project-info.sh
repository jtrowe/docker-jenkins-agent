# .project-info.sh

# Contains project information as shell variables.

export PROJECT_AUTHOR="Joshua T. Rowe <jrowe@jrowe.org>"
export PROJECT_DESCRIPTION="A customized image providing a Jenkins agent."
export PROJECT_LICENSE=""
export PROJECT_NAME="docker-jenkins-agent"
export PROJECT_REPOSITORY="http://gitlab.com/jtrowe/docker-jenkins-agent.git"
export PROJECT_TITLE="Jenkins-Agent"
export PROJECT_URL="http://gitlab.com/jtrowe/docker-jenkins-agent"
export PROJECT_VENDOR="jrowe.org"
export PROJECT_VERSION="1.0"

