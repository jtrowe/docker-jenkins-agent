
MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables

.DELETE_ON_ERROR:

SHELL := bash


owner      := jrowe
image      := jenkins-agent

commit        := $(shell ci/bin/get-latest-commit)
branch        := $(shell ci/bin/get-branch-name)
branch_tag    := $(shell echo "$(branch)" | ci/bin/normalize-docker-tag)
cache_version := $(branch_tag)
image_version := $(branch_tag)


.PHONEY: build
build: .image.json


.PHONEY: info
info:
	@ echo "commit        => $(commit)"
	@ echo "branch        => $(branch)"
	@ echo "branch_tag    => $(branch_tag)"
	@ echo "cache_version => $(cache_version)"
	@ echo "image_version => $(image_version)"
	@ echo "image         => $(image)"


.image.json: .FORCE
	ci-scripts/bin/install-ci-scripts ci/bin
	ci/bin/docker-build -c $(owner)/$(image):$(cache_version) -i $@ $(image):$(image_version) .
	docker inspect $(image):$(image_version) > $@


# NB: old target for reference
.image.json.old: .FORCE
	@ ci-scripts/bin/install-ci-scripts ci/bin
	DOCKER_REGISTRY=$(registry) \
	ci/bin/docker-build -c $(owner)/$(image)/$(branch_tag) -i $@ $(image):$(commit) . ; \
	DOCKER_REGISTRY=$(registry) \
	ci/bin/docker-push $(image):$(commit) $(owner)/$(image) $(commit) $(branch_tag)


.PHONEY: show.labels
show.labels:
	docker image inspect --format='{{json .Config.Labels}}' $(image):$(image_version) | json_pp


.FORCE:


